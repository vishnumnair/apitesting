
# **Rest Api Testing framework using Rest Assured**
This Rest Assured based API testing framework is designed to script test cases for OpenWeather API using BDD and Java.

#   Table of Contents

* [Overview](#overview)
* [How to setup](#howtosetup)
* [How to run tests and generate reports](#howtoruntests)
* [Where to find Reports](#reports)
* [Jenkins (CI/CD) Integration setup guideline](#jenkinsCICD)

<a name="overview"></a>
## 1. Overview 

* Language : Java
* Reporting      : Extent Report<br>
* Framework model: BDD using Cucumber<br>
* Output types: Json,XML and HTML<br>
* Output folder: ./target/cucumber-reports<br>
* Extent report location: 'APIReport.html' Automatically generated under root folder

* Sonar Report:

![Image](./screenshots/sonar.png)


<a name="howtosetup"></a>
## 2. Set up the framework
___

Clone source code from Git.

Once cloned, the tests can be run as follows:

> Note: Git client and Maven are required to setup and run
> * [Git Installation](https://www.atlassian.com/git/tutorials/install-git)
> * [Apache Maven Installation](http://maven.apache.org/install.html/) 
--- 

<a name="howtoruntests"></a>

## 3. Run tests and generate reports
 
 Run the following command in cmd to build and execute tests

```batch
mvn test -Dcucumber.options="--tags @Regression"
```

This command will run scenarios with tag as '@Regression' in the feature file.

>Note: Running the command <code>mvn test</code> without specifying the tests will result in running the default tags mentioned in the RunnerClass

Results will be stored as reports in ./target/cucumber-reports/* folder along with the APIReport.html in the root folder.

---

<a name="reports"></a>
## 4. Finding reports

* HTML file: ./APIReport.html
* JSON file: ./target/cucumber-reports/Cucumber.json
* XML file: ./target/cucumber-reports/Cucumber.xml

---
<a name="jenkinsCICD"></a>
## 5. Jenkins (CI/CD) Integration setup

*TO BE  ADDED
---
