package com.framework.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

/**
 * The type Boot strap.
 */
public class Reporter {
    private static Reporter bootStrap;
    private ExtentReports extent = new ExtentReports();
    private ExtentTest extentTestNode;
    private ExtentTest extentStepNode;

    private Reporter() {
        ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter("ApiReport.html");
        extentSparkReporter.config().setTheme(Theme.DARK);
        extentSparkReporter.config().setDocumentTitle("APITesting Report");
        extent.attachReporter(extentSparkReporter);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static Reporter getInstance() {
        if (bootStrap == null) {
            bootStrap = new Reporter();
        }
        return bootStrap;
    }

    /**
     * Create test boot strap.
     *
     * @param feature  the feature
     * @param scenario the scenario
     * @return the boot strap
     */
    public Reporter createTest(String feature, String scenario) {
        ExtentTest extentTest = extent.createTest(feature);
        extentTestNode = extentTest.createNode(scenario);
        extent.flush();
        return this;
    }

    /**
     * Create bdd step.
     *
     * @param stepDetails the step details
     */
    public void createBDDStep(String stepDetails) {

        this.extentStepNode = extentTestNode.createNode(stepDetails);

    }

    /**
     * Gets logger.
     *
     * @return the logger
     */
    public ExtentTest getLogger() {
        return extentTestNode;
    }

    /**
     * Gets step logger.
     *
     * @return the step logger
     */
    public ExtentTest getStepLogger() {
        return extentStepNode;
    }

    /**
     * Sets step logger.
     *
     * @param extentStepNode the extent step node
     */
    public void setStepLogger(ExtentTest extentStepNode) {
        this.extentStepNode = extentStepNode;
    }

    /**
     * Flush report.
     */
    public void flushReport() {
        extent.flush();
    }
}
