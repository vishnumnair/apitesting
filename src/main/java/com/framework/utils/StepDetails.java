package com.framework.utils;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.EventHandler;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestStepStarted;

/**
 * The type Step details.
 */
public class StepDetails implements ConcurrentEventListener {
    /**
     * The constant stepName.
     */
    public static String stepName;

    /**
     * The Step handler.
     */
    public EventHandler<TestStepStarted> stepHandler = event -> handleTestStepStarted(event);

    @Override
    public void setEventPublisher(EventPublisher publisher) {
        publisher.registerHandlerFor(TestStepStarted.class, stepHandler);
    }

    private void handleTestStepStarted(TestStepStarted event) {
        if (event.getTestStep() instanceof PickleStepTestStep) {
            PickleStepTestStep testStep = (PickleStepTestStep) event.getTestStep();
            stepName = testStep.getStep().getText();
        }


    }
}
