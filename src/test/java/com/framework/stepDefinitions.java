package com.framework;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.CodeLanguage;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.framework.utils.Reporter;
import com.framework.utils.StepDetails;
import com.google.gson.JsonObject;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class stepDefinitions {

    protected static Response currentResponse;
    private static RequestSpecification request;
    private ExtentTest stepLogger;


    @Given("^I have to make API calls to \"([^\"]*)\" with query Params$")
    public void api_Call_Specification(String URL, DataTable table) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();
        stepLogger.info("Base URI:" + baseURI);
        RestAssured.baseURI = URL;
        request = given().contentType(ContentType.JSON);
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        for (Map<String, String> dataMap : data) {
            request.queryParam(dataMap.get("query_key"), dataMap.get("query_value"));
        }
        stepLogger.pass("Request built successfully");
    }

    @When("^I register a station with the following values and without an API Key to endpoint \"([^\"]*)\"$")
    public void register_Station_noAPIKey(String endpoint, DataTable table) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();

        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        JsonObject requestJson = new JsonObject();
        stepLogger.info("Building the POST request");

        for (Map<String, String> dataMap : data) {
            requestJson.addProperty("external_id", dataMap.get("external_id"));
            requestJson.addProperty("name", dataMap.get("name"));
            requestJson.addProperty("latitude", Float.parseFloat(dataMap.get("latitude")));
            requestJson.addProperty("longitude", Float.parseFloat(dataMap.get("longitude")));
            requestJson.addProperty("altitude", Float.parseFloat(dataMap.get("altitude")));
        }

        currentResponse = request.with().body(requestJson).when().post(endpoint);
        stepLogger.pass("Request posted successfully");
        stepLogger.info("Response is:");
        stepLogger.info(MarkupHelper.createCodeBlock(currentResponse.getBody().prettyPrint(), CodeLanguage.JSON));
    }

    @Then("^I assert the response body with \"([^\"]*)\" as \"([^\"]*)\"$")
    public void assert_ResponseBody(String key, String expectedValue) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();

        stepLogger.info("Validating the response");
        String actualValue = currentResponse.jsonPath().get(key).toString();
        assertEquals("ERROR: Values does not match" + actualValue, expectedValue, actualValue);

        stepLogger.pass("Validation passed");
    }

    @When("^I register a new station in OpenWhetherMap with values \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" to endPoint \"([^\"]*)\"$")
    public void register_Station(String external_id, String name, String latitude, String longitude, String altitude, String endpoint) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();

        stepLogger.info("Building POST request");
        JsonObject requestJson = new JsonObject();
        requestJson.addProperty("external_id", external_id);
        requestJson.addProperty("name", name);
        requestJson.addProperty("latitude", Float.parseFloat(latitude));
        requestJson.addProperty("longitude", Float.parseFloat(longitude));
        requestJson.addProperty("altitude", Float.parseFloat(altitude));
        stepLogger.info(MarkupHelper.createCodeBlock(requestJson.toString(), CodeLanguage.JSON));
        currentResponse = request.with().body(requestJson).when().post(endpoint);

        stepLogger.pass("New Station registered successfully with externalID:" + external_id);
        stepLogger.info("Response is:");
        stepLogger.info(MarkupHelper.createCodeBlock(currentResponse.getBody().prettyPrint(), CodeLanguage.JSON));
    }


    @Then("^I have received HTTP response code of \"([^\"]*)\"$")
    public void assert_Response_Code(int responseCode) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();
        stepLogger.info("Validating the response");

        assertEquals("ERROR: The actual Code is " + currentResponse.getStatusCode(), responseCode, currentResponse.getStatusCode());
        stepLogger.pass("Validation passed");
    }


    @Then("^I receive station details using GET request with endpoint \"([^\"]*)\"$")
    public void receive_Station_Details(String endpoint) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();

        stepLogger.info("Sending GET request");
        currentResponse = request.get(endpoint + "/" + currentResponse.jsonPath().get("ID").toString());
        stepLogger.info("Response is:");
        stepLogger.info(MarkupHelper.createCodeBlock(currentResponse.getBody().prettyPrint(), CodeLanguage.JSON));
        stepLogger.pass("GET request sent successfully");
    }

    @Then("^I check for new station of values \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void verify_In_Db(String external_ID, String name, String latitude, String longitude, String altitude) {
        Reporter.getInstance().createBDDStep(StepDetails.stepName);
        stepLogger = Reporter.getInstance().getStepLogger();

        stepLogger.info("Validating the new station registered");

        assertEquals("ERROR: The actual external_ID is " + currentResponse.jsonPath().get("external_id"), external_ID, currentResponse.jsonPath().get("external_id"));
        assertEquals("ERROR: The actual name is " + currentResponse.jsonPath().get("name"), name, currentResponse.jsonPath().get("name"));
        assertEquals("ERROR: The actual latitude is " + currentResponse.jsonPath().get("latitude").toString(), latitude, currentResponse.jsonPath().get("latitude").toString());
        assertEquals("ERROR: The actual longitude is " + currentResponse.jsonPath().get("longitude").toString(), longitude, currentResponse.jsonPath().get("longitude").toString());
        assertEquals("ERROR: The actual altitude is " + currentResponse.jsonPath().get("altitude").toString(), altitude, currentResponse.jsonPath().get("altitude").toString());

        stepLogger.pass("Validation passed");
    }


}
