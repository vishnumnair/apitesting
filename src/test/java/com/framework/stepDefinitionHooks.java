package com.framework;

import com.framework.utils.Reporter;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class stepDefinitionHooks {
    @Before
    public void before(Scenario scenario) {
        Reporter.getInstance().createTest(scenario.getName(), scenario.getName());
    }

}
