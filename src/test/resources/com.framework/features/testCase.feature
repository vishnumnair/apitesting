@API_Testing
Feature: OpenWeatherMap API Testing

  @Negative @Regression
  Scenario: Register weather station without API Key and expect negative response
    Given I have to make API calls to "http://api.openweathermap.org/data/3.0" with query Params
      | query_key     | query_value |
      | appid         |             |
    When I register a station with the following values and without an API Key to endpoint "/stations"
      | external_id   | name              | latitude | longitude | altitude |
      | noAPIKey      | negative_scenario | 123.4    | 456.7     | 789.0    |

    Then I assert the response body with "cod" as "401"
    Then I assert the response body with "message" as "Invalid API key. Please see http://openweathermap.org/faq#error401 for more info."


    @Positive @Regression
    Scenario Outline: Register Station and validate Response
      Given I have to make API calls to "http://api.openweathermap.org/data/3.0" with query Params
          | query_key     | query_value                      |
          | appid         | 0623c0384be7a84ca35bda3dd23b7db5 |
      When I register a new station in OpenWhetherMap with values "<external_id>","<name>","<latitude>","<longitude>","<altitude>" to endPoint "/stations"
      Then I have received HTTP response code of "201"
      When I receive station details using GET request with endpoint "/stations"
      Then I check for new station of values "<external_id>","<name>","<latitude>","<longitude>","<altitude>"
      Examples:
        | external_id  | name                       | latitude | longitude | altitude |
        | DEMO_TEST001 | Team Demo Test Station 001 | 33.33    | -122.43   | 222      |
        | DEMO_TEST002 | Team Demo Test Station 002 | 44.44    | -122.44   | 111      |

